# alta3research-ansible-cert

## Play Description

Proof of concept for managing records in NetBox, a Data Center Infomation Management software. This playbook will create a switch and assign a primary IP Address based on variables in vars/new-device.yml. Variables have been predefined in the vars files and can be run as is to test the playbook. This playbook relies on the public demo of NetBox. Instructions for creating an account to use this playbook are below. 

## Files
- alta3research-ansiblecert01.yml - Playbook File
- README.md - This Document
- vars
  - new-device.yml - Variable File for all variables required to run the playbook
  - token.yml - Variable File used to cache the API Token

## Prerequisites:

- Ansible 2.12+
  - Ansible Collection - netbox.netbox version 3.16.0
- Python 3.8+
  - Python Module - pytz
  - Python Module - pynetbox
- Completed Daily - Create a NetBox username/password and generate an API token in the public Demo Environment

## Instructions to Create a NetBox Username, Password, and API Token in the public demo environment

Note: The NetBox Demo Environment is reset every day at 4:00 UTC - These steps need to be completed daily
1. Navigate to https://demo.netbox.dev/plugins/demo/login/
2. Enter a random Username and Password
3. Click Create & Sign In
4. Click your username in the top right
5. Click API Tokens
6. Click Add a Token
7. Copy and Save the key - This will be provided in a prompt when running the playbook
8. Click Create

## Variables

- netboxUrl: URL of the netbox instance, required, hard coded
- netboxToken: API Token obtained within netbox, required, prompted on run
- netboxTokenSave: API Token saved after a successful run, located in token.yml variable file
- netboxDeviceName: Name of the device to be added or updated, required, provide in new-device.yml variable file
- netboxDeviceIp: Primary management IP Address for the devicew ith CIDR, required, provide in new-device.yml variable file
- netboxDeviceMgmtInterface: Interface the primary management IP Address is attached to, required, provide in new-device.yml variable file
- netboxDeviceType: Model of the device, required, provide in new-device.yml variable file, must match a value already in the environment
- netboxDeviceRole: Role of the device, required, provide in new-device.yml variable file, must match a value already in the environment
- netboxDeviceSite: Site the device is located at, required, provide in new-device.yml variable file, must match a value already in the enviornment

## Note on NetBox API Token

- This play prioritizes convienence over security. The API token will be stored unencrypted in vars/token.yml.
- The play includes a prompt for providing the API token. The token will be stored after a successful run.
- The Stored API token will be the default for the prompt = Easier to run multiple times in one day.
- In a production environment, use Ansible Vault for the API Token. Remove prompt, vars/token.yml, and last task.

